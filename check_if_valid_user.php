<?php

    session_start();

    if (array_key_exists("user", $_SESSION)) {
        echo json_encode(
            array(
                "validUserLoggedIn" => true,
                "user" => $_SESSION['user']
            )
        );
    }else {        
        echo json_encode(
            array(
                "validUserLoggedIn" => false,
                "user" => null
            )
        ); 
            
    }
    
?>
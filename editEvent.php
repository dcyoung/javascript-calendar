<?php

	include_once('database.php');
	header("Content-Type: application/json");
	
	session_start();
	ini_set("session.cookie_httponly", 1);
	$username = $_SESSION['user'];
	
	//escape output
	$title = mysql_real_escape_string( htmlentities ($_POST["eventname"] ));
	$hour = mysql_real_escape_string( htmlentities ($_POST["hour"] ));
	$day = mysql_real_escape_string( htmlentities ($_POST["day"] ));
		
	//could use prepare instead
	$sql = "UPDATE events SET time='$hour' WHERE associated_username='$username' AND date='$day' AND title='$title'";
	    
	$result = mysql_query($sql);
    
	if( $result ) {
		echo json_encode(
			array(
				"eventEdit" => true,
				"user" => $_SESSION['user'],
				"eventname" => $title
			)
		);
		exit();
	} else {
	    echo json_encode(
		    array(
			    "eventEdit" => false,
			    "message" => "Failed to edit the event specified by the provided name."
		    )
	    );
	    exit();
	}

?>
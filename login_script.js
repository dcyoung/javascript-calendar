/* Add event listeners to buttons */
$(document).ready(function() {
    $("#login_button").click(loginAjax);
    $("#logout_button").click(logoutAjax);
    $("#register_new_user_button").click(registerAjax);
    
    verify_user_credentials_Ajax();
    $("#register_button").click(function() {
        $("#login").hide();
        $("#register").show();
    });
});

/* Verify user creditials*/
function verify_user_credentials_Ajax(event) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "check_if_valid_user.php", true);
    xmlHttp.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
    xmlHttp.addEventListener("load", verify_user_credentials_Callback, false);
    xmlHttp.send(null);
}

function verify_user_credentials_Callback(event) {
        var jsonData = JSON.parse(event.target.responseText);
        var validUserLoggedIn = jsonData.validUserLoggedIn;
        var userName = jsonData.user;
        if (validUserLoggedIn) {
            //Hide the sections not needed (login and registration sections, and display the calendar)
            $("#login").hide();
            $("#register").hide();
            $("#welcome").show();
            $("#welcome_message_section").html("Welcome " + jsonData.user);
            $("#calendar").show();
	    $("#dayLabelsClass").show();
	    $("#todaysEventsDisplay").show();
        } else {
            //Show login, hide signup, welcome and calendar, 
            $("#login").show();
            $("#register").hide();
            $("#welcome").hide();
            $("#calendar").hide();
	    $("#dayLabelsClass").hide();
	    $("#todaysEventsDisplay").hide();
        }
    }

/* Login Ajax*/
function loginAjax(event) {
    var username = $("#username").val(); // Get the username from the form
    var password = $("#password").val(); // Get the password from the form
    // Make a URL-encoded string for passing POST data:
    var dataString = "username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password);
    var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
    xmlHttp.open("POST", "login.php", true);
    xmlHttp.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
    xmlHttp.send(dataString); // Send the data
    xmlHttp.addEventListener("load", loginCallback, false);
    //Clear the fields
    $("#username").val("");
    $("#password").val("");
}

function loginCallback(event) {
        var jsonData = JSON.parse(event.target.responseText);
        if (jsonData.success) {
            //Hide login and signup, show welcome and calendar
            $("#login").hide();
            $("#register").hide();
            $("#welcome").show();
            $("#welcome_message_section").html("Welcome " + jsonData.user);
            $("#calendar").show();
	    $("#dayLabelsClass").show();
	    $("#todaysEventsDisplay").show();
            $("#event").hide();
            $("#loginFeedbackMessage").html("");
            //calendarInit();
            location.reload();
        } else {
            $("#loginFeedbackMessage").html("You were not logged in.  " + jsonData.message);
        }
    }
    /* Logout */

function logoutAjax(event) {
    var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
    xmlHttp.open("POST", "logout.php", true);
    xmlHttp.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
    xmlHttp.send(null); // Send the data
    xmlHttp.addEventListener("load", logoutCallback, false);
}

function logoutCallback(event) {
        //Show login, hide signup, welcome and calendar, 
        $("#login").show();
        $("#register").hide();
        $("#welcome").hide();
        $("#calendar").hide();
	$("#dayLabelsClass").hide();
	$("#todaysEventsDisplay").hide();
        $("#event").hide();
    }
    /* New User*/

function registerAjax(event) {
    var username = $("#reg_username").val(); // Get the username from the form
    var password = $("#reg_password").val(); // Get the password from the form
    var email = $("#reg_email").val(); // Get the password from the form
    // Make a URL-encoded string for passing POST data:
    var dataString = "username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password) + "&email=" +
        encodeURIComponent(email);
    var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
    xmlHttp.open("POST", "registerNewUser.php", true);
    xmlHttp.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
    xmlHttp.send(dataString); // Send the data
    xmlHttp.addEventListener("load", registerCallback, false);
    //Clear the fields
    $("#reg_username").val("");
    $("#reg_password").val("");
    $("#reg_email").val("");
}

function registerCallback(event) {
    var jsonData = JSON.parse(event.target.responseText);
    if (jsonData.success) {
        //hide login and signup, show welcome and calendar, 
        $("#login").hide();
        $("#register").hide();
        $("#welcome").show();
        $("#welcome_message_section").html("Welcome " + jsonData.user);
        $("#event").hide();
        $("#calendar").show();
	$("#dayLabelsClass").show();
	$("#todaysEventsDisplay").show();
        location.reload();
    } else {
        $("#loginFeedbackMessage").html("You were not registered.  " + jsonData.message);
    }
}
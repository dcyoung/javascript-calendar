<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style_sheet.css" />
        <script src="//cdn.sencha.io/ext-4.2.0-gpl/ext.js"></script>
        
        <script src="jquery.min.js"></script>
        <script src="jquery-ui.min.js"></script>
        
        <script type="text/javascript" src="login_script.js"></script>
        <script type="text/javascript" src="calendar_to_refer.js"></script>
        <script type="text/javascript" src="calendar_main_script.js"></script>
               
        <title>JS Calendar</title>
    </head>
    
    <body>
        
        <h2>Javascript Calendar</h2>
        <?php
            session_start();
            include_once('database.php');
        ?>
    
        <div id="login" class="login" >  
                <h3>Login</h3>
                <label>Username:</label>
                <input id="username" type="text" name="username" ><br/>
                
                <label>Password:</label>
                <input id="password" type="password" name="password"><br/>
                
                <button id="login_button">login</button>
                <button id="register_button">Sign up</button>
        </div>
        
        <div id="register" class="login" >
                <h3>Sign up</h3>    
        
                <label>Username:</label>
                <input id="reg_username" type="text" name="username" ><br/>
          
                <label>Password:</label>
                <input id="reg_password" type="password" name="password"><br/>
          
                <label>Email Address:</label>
                <input id="reg_email" type="text" name="email"><br/>
          
                <button id="register_new_user_button">register</button>
        </div>
          
       
        <div id="loginFeedbackMessage" class="login"></div>
        
        <div class="login" id="welcome">
                <div id="welcome_message_section">Welcome</div>
                <button id="logout_button">logout</button>
                <p id="userFeedbackMessage"></p>
        </div>
        <br>
        <div id= "todaysEventsDisplay">
            <p id= "instructions" >
                The calendar shows the events scheduled each day of the current month.      <br>      
                You may click on a day to add or edit the events scheduled for that day.<br>
                To add a new event, simply enter a title and a time and then hit "Create new Event"<br>
                To edit the time of an existing event, enter the event's title and select a new time before hitting "edit".<br>
                To delete an event, simply enter the event's title and hit "delete".
            </p>
             <div id = "weatherBox">
                <strong>Today's Weather</strong><br>
                <div class="weather" id="weatherWidget">
                    <div class="weather-loc" id= "location"></div>
                    <div class="weather-humidity" id="humidity"></div>
                    <div class="weather-temp" id="temp"></div>
                    <img  id= "tomorrow_image" alt="Smiley face" src="http://us.yimg.com/i/us/nws/weather/gr/32ds.png">
                </div>
            </div>
            <div id = "todaysEventsBox">
                <strong> List of Events for Today's Date</strong><br>
                <div id="actualEventsToday"></div>
            </div>
            <br>
        </div>
        
        <br>
        <div id="dayLabelsClass">
            Below are the month's events.<br>
                
            <button id="decrement_month_button">Previous Month</button> 
            <strong><span id="currentMonth"></span></strong>
            <button id="increment_month_button">Next Month</button>
            <table id="dayLabels" class="dayLabelsClass">
                <tr class="daysOfWeek">
                       <td class="d0">Sunday</td>
                       <td class="d1">Monday</td>
                       <td class="d2">Tuesday</td>
                       <td class="d3">Wednesday</td>
                       <td class="d4">Thursday</td>
                       <td class="d5">Friday</td>
                       <td class="d6">Saturday</td>
               </tr>
            </table>        
            
        </div>
    
        <div id="calendar">
            
                <!--Below are the month's events.<br>-->
                <!---->
                <!--<button id="decrement_month_button">Previous Month</button> -->
                <!--<strong><span id="currentMonth"></span></strong>-->
                <!--<button id="increment_month_button">Next Month</button>-->
                
                <table id="calendarTable" class="calendar">
                    <tr class="week0">
                        <td class="day0"></td>
                        <td class="day1"></td>
                        <td class="day2"></td>
                        <td class="day3"></td>
                        <td class="day4"></td>
                        <td class="day5"></td>
                        <td class="day6"></td>
                    </tr>
                    <tr class="week1">
                        <td class="day0"></td>
                        <td class="day1"></td>
                        <td class="day2"></td>
                        <td class="day3"></td>
                        <td class="day4"></td>
                        <td class="day5"></td>
                        <td class="day6"></td>
                    </tr>
                    <tr class="week2">
                        <td class="day0"></td>
                        <td class="day1"></td>
                        <td class="day2"></td>
                        <td class="day3"></td>
                        <td class="day4"></td>
                        <td class="day5"></td>
                        <td class="day6"></td>
                    </tr>
                    <tr class="week3">
                        <td class="day0"></td>
                        <td class="day1"></td>
                        <td class="day2"></td>
                        <td class="day3"></td>
                        <td class="day4"></td>
                        <td class="day5"></td>
                        <td class="day6"></td>
                    </tr>
                    <tr class="week4">
                        <td class="day0"></td>
                        <td class="day1"></td>
                        <td class="day2"></td>
                        <td class="day3"></td>
                        <td class="day4"></td>
                        <td class="day5"></td>
                        <td class="day6"></td>
                    </tr>
                </table>
            
        </div>
        
        <div id="Popup-Event-Dialog-Box" title="Confirm">
        
            <div id="event" class="login">
                
                <strong>Add,Delete,or Edit Event</strong>
                <br><label>Title:</label>
                <input id="event_title" type="text" name="eventname" maxlength="20">
                
                <input type="hidden" id="day" name = "day" value="">
                                
                <br><label>Time:</label>
                <select id="hour" name="hour">
                        <option selected="selected" value="0:00:00">0:00</option>
                        <option value="1:00:00">1:00</option>
                        <option value="2:00:00">2:00</option>
                        <option value="3:00:00">3:00</option>
                        <option value="4:00:00">4:00</option>
                        <option value="5:00:00">5:00</option>
                        <option value="6:00:00">6:00</option>
                        <option value="7:00:00">7:00</option>
                        <option value="8:00:00">8:00</option>
                        <option value="9:00:00">9:00</option>
                        <option value="10:00:00">10:00</option>
                        <option value="11:00:00">11:00</option>
                        <option value="12:00:00">12:00</option>
                        <option value="13:00:00">13:00</option>
                        <option value="14:00:00">14:00</option>
                        <option value="15:00:00">15:00</option>
                        <option value="16:00:00">16:00</option>
                        <option value="17:00:00">17:00</option>
                        <option value="18:00:00">18:00</option>
                        <option value="19:00:00">19:00</option>
                        <option value="20:00:00">20:00</option>
                        <option value="21:00:00">21:00</option>
                        <option value="22:00:00">22:00</option>
                        <option value="23:00:00">23:00</option>
                </select>
                <br>
                <input type="hidden" id="token" name="token" value="<?php echo $_SESSION['token'];?>" />
                
                <button id="create_new_event_button">Create New Event</button>
                <button id="delete_existing_event_button">Delete</button>
                <button id="edit_existing_event_button">Edit</button>
                <br>
                <strong>Scheduled Events:</strong>                
            </div>
        
            <div id="events_for_the_day" class="login"></div>           
            
        </div>
        
        
       <!-- <div id= "todaysEventsDisplay">
            <div id = "todaysEventsBox">
                <strong> List of Events for Today's Date</strong><br>
                <div id="actualEventsToday"></div>
            </div>
            <div id = "weatherBox">
                <strong>Today's Weather</strong><br>
                <div class="weather" id="weatherWidget">
                    <div class="weather-loc" id= "location"></div>
                    <div class="weather-humidity" id="humidity"></div>
                    <div class="weather-temp" id="temp"></div>
                    <img  id= "tomorrow_image" alt="Smiley face" src="http://us.yimg.com/i/us/nws/weather/gr/32ds.png">
                </div>
            </div>
        </div>-->
        
        <script>
            //Hide elements immediately to avoid blinking on page-load
            $("#login").hide();
            $("#register").hide();
            $("#welcome").hide();
            $("#calendar").hide();
	    $("#dayLabelsClass").hide();
            $("#event").hide();
        </script>
    
    </body>
</html>

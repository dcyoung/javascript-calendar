<?php
    include_once('database.php');
    header("Content-Type: application/json");
    
    session_start();
    ini_set("session.cookie_httponly", 1);
    $username = $_SESSION['user'];
    
    $date = mysql_real_escape_string(htmlentities( $_POST["day"]) );
    $w = $_POST["w"];
    $d = $_POST["d"];
        
    $sql = "SELECT title, time FROM events WHERE (associated_username='$username' AND date='$date') ORDER BY time ASC";
    $res = mysql_query($sql);
    $events = array();
    
    //create an array of events where each element is an event row from the query
    while($event_instance = mysql_fetch_assoc($res)) {
	$events[] = $event_instance;
    }
    
    
    if($events != null) {
	echo json_encode(
	    array(
		"eventExisted" => true,
		"events" => $events,
		"w" => $w,
		"d" => $d
	    )
	);
	exit();
    } else {
	echo json_encode(
	    array(
		"eventExisted" => false,
		"message" => "No events exist",
		"events" => $events
	    )
	);
	exit();
    }  

?>
<?php
    session_start();
    unset($_SESSION['username']);
    unset($_SESSION['user']);
    unset($_SESSION['token']);
    session_destroy();
?>
<?php

    include_once('database.php');
    header("Content-Type: application/json");
    
    session_start();
    ini_set("session.cookie_httponly", 1);
    $username = $_SESSION['user'];
    
    $title = mysql_real_escape_string( htmlentities ($_POST["eventname"] ));
    $hour = mysql_real_escape_string( htmlentities ($_POST["hour"] ));
    $day = mysql_real_escape_string( htmlentities ($_POST["day"] ));

    $sql = "INSERT INTO events (associated_username, title, date, time) VALUES('$username', '$title', '$day', '$hour');";
	
    $result = mysql_query($sql);
    
    if( $result ) {
        echo json_encode(
            array(
                "eventAdded" => true,
                "user" => $_SESSION['user'],
                "eventname" => $title
            )
        );
	exit();
    } else {
	echo json_encode(
            array(
                "eventAdded" => false,
                "message" => "Failed to Create the Event"
            )
        );
	exit();
    }

?>
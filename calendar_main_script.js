   var csrf_token = '<%= token_value %>';
   $("body").bind("ajaxSend", function(elm, xhr, s) {
       if (s.type == "POST") {
           xhr.setRequestHeader('X-CSRF-Token', csrf_token);
       }
   });
    // Global variables
   var currentMonth = new Month(2014, 10); // October 2014
   var currently_Selected_Cell = null;
   var update_date; //date currently having events retrieved
   var global_date;
   
   $(document).ready(function() {
       //Initialize the calendar
       updateCalendar();
       initializePopupDialogBox();
       
       
       // Change the month
       $("#increment_month_button").click(function() {
           currentMonth = currentMonth.nextMonth();
           updateCalendar(); 
       });
       $("#decrement_month_button").click(function() {
           currentMonth = currentMonth.prevMonth();
           updateCalendar();
       });
       
       $('#calendar td').mouseover(function() {
           $(this).addClass('hover');
           currently_Selected_Cell = $(this);
       });
       $('#calendar td').mouseout(function() {
           $(this).removeClass('hover');
           currently_Selected_Cell = null;
       });
       $('#calendar td').click(function() {
           //if a day on the calendar is clicked, find its date... 
           var date = $(this).html().slice(0, 2);
           var week = $(this).parent().attr('class').slice(-1);
           //Determine the correct month by accounting for rollover dates from previous and next month
           //then open the eventDialog box to describe the events for that date
           if (week == 0 && date > 20) {
               openPopupDialogBox((currentMonth.month - 1 % 12), date);
           } else if (week == 4 && date < 10) {
               openPopupDialogBox((currentMonth.month + 1 % 12), date);
           } else {
               openPopupDialogBox(currentMonth.month, date);
           }
       });
       $("#create_new_event_button").click(function() {
           createNewEventAjax();
       });
       $("#delete_existing_event_button").click(function() {
           deleteExistingEventAjax();
       });
       $("#edit_existing_event_button").click(function() {
           editExistingEventAjax();
       });
       
   });
    // This updateCalendar() function only alerts the dates in the currently specified month.  You need to write
    // it to modify the DOM (optionally using jQuery) to display the days and weeks in the current month.
   function updateCalendar() {
       $("#currentMonth").html(monthToString(currentMonth.month) + " " + currentMonth.year);
       var weeks = currentMonth.getWeeks();
       for (var w in weeks) {
           var days = weeks[w].getDates();
           for (var d in days) {
               if (w == 0 && days[d].getDate() > 20) {
                     var currentMonth_temp = currentMonth.prevMonth();
                     $(".week" + w).find(".day" + d).html(days[d].getDate());    //draws the day number into the calendar day slot (1-31)
                     $(".week" + w).find(".day" + d).click(function() {          //adds a clickable function
                        //retrieves the events for a given day to list in the dialog box only if a day is clicked
                         var today = currentMonth.prevMonth().year + "-" + currentMonth.prevMonth().month + "-" +$(this).html();
                         var day = document.getElementById("day");
                         day.setAttribute('value', today);
                         
                         retrieveAllEvents();
                         $("#event").show();
                     });
                     //call to draw the events in each calendar slot, regardless of if they'e been clicked
                     update_date = currentMonth_temp.year + "-" + currentMonth_temp.month + "-" + days[d].getDate();
                     check_for_events(w, d);               
                     
               } else if (w == 4 && days[d].getDate() < 10) {
                     var currentMonth_temp = currentMonth.nextMonth();
                     $(".week" + w).find(".day" + d).html(days[d].getDate());    //draws the day number into the calendar day slot (1-31)
                     $(".week" + w).find(".day" + d).click(function() {          //adds a clickable function
                        //retrieves the events for a given day to list in the dialog box only if a day is clicked
                         var today = currentMonth.nextMonth().year + "-" + currentMonth.nextMonth().month + "-" +$(this).html();
                         var day = document.getElementById("day");
                         day.setAttribute('value', today);
                         retrieveAllEvents();
                         $("#event").show();
                     });
                     //call to draw the events in each calendar slot, regardless of if they'e been clicked
                     update_date = currentMonth_temp.year + "-" + currentMonth_temp.month + "-" + days[d].getDate();
                     check_for_events(w, d);
               } else {
                     var currentMonth_temp = currentMonth;
                     $(".week" + w).find(".day" + d).html(days[d].getDate());    //draws the day number into the calendar day slot (1-31)
                     $(".week" + w).find(".day" + d).click(function() {          //adds a clickable function
                        //retrieves the events for a given day to list in the dialog box only if a day is clicked
                         var today = currentMonth.year + "-" + currentMonth.month + "-" +$(this).html();
                         var day = document.getElementById("day");
                         day.setAttribute('value', today);
                         
                         retrieveAllEvents();
                         $("#event").show();
                     });
                     
                     
                     update_date = currentMonth_temp.year + "-" + currentMonth_temp.month + "-" + days[d].getDate();
                     
                     var update_dateOBJ = new Date(currentMonth_temp.year, currentMonth_temp.month, days[d].getDate());
                     //draw a holiday if it exists
                     var holiday = check_holiday(update_dateOBJ);
                     if(holiday != "false"){
                        drawHoliday(w,d,holiday);
                     }
                     //call to draw the events in each calendar slot, regardless of if they'e been clicked
                     check_for_events(w, d);
                     
               }
           }
       }
       drawTodaysEventDisplay();
   }
   
   //check for events for a specific day currently being updated as updateCalender iterates through every day of the month
   function check_for_events(w, d) {
       var dataString = "day=" + encodeURIComponent(update_date) + "&w=" + encodeURIComponent(w) + "&d=" + encodeURIComponent(d);
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("POST", "checkForEvents.php", true);
       xmlHttp.setRequestHeader("Content-Type",
           "application/x-www-form-urlencoded");
       xmlHttp.addEventListener("load", checkForEventsCallback, false);
       xmlHttp.send(dataString);
   }
   //check for events for a specific day currently being updated as updateCalender iterates through every day of the month
   //draw the events for a specific day in the calendar table box
   function checkForEventsCallback(event) {
       var jsonData = JSON.parse(event.target.responseText);
       if (jsonData.eventExisted) {
           var w = jsonData.w;
           var d = jsonData.d;
           var events = jsonData.events;
           for (var i in events) {
               var title = events[i].title;
               var time = events[i].time;
               var eventHTML =
                   " <div class='calEvent'><p>" +
                   title + " " + time + "</p>" + "<br></div>";
               $(".week" + w).find(".day" + d).append(eventHTML);
           }
       }
   }
   
   function drawHoliday(w,d,h) {
      var eventHTML =
          " <div class='holEvent'><p>" +
          h + "</p>" + "<br></div>";
      $(".week" + w).find(".day" + d).append(eventHTML);
   }
   
   function initializePopupDialogBox() {
       $("#Popup-Event-Dialog-Box").dialog({
           title: 'Edit Event',
           autoOpen: false,
           draggable: true,
           resizable: true,
           width: 300,
           height: 500,
       });
   }

   function openPopupDialogBox(month, date) {
       //var title = 'Editing Event';
       var dialogDiv = $('#Popup-Event-Dialog-Box');
       dialogDiv.dialog("option", "position", 'center');
       dialogDiv.dialog("option", "title", "Events For: " + monthToString(
           month) + " " + date); //Add New Event On
       dialogDiv.dialog('open');
       dialogDiv.parent().css("z-index", "1100");
   }
   
   //create a new event
   function createNewEventAjax() {
       var eventname = $("#event_title").val();
       var h = document.getElementById("hour");
       var hour = h.options[h.selectedIndex].value;
       var day = "";
       var day = document.getElementById("day").value;
       var dataString = "eventname=" + encodeURIComponent(eventname) +
           "&day=" + encodeURIComponent(day) +
           "&hour=" + encodeURIComponent(hour);
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("POST", "addEvent.php", true);
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       xmlHttp.send(dataString); 
       xmlHttp.addEventListener("load", createNewEventCallback, false);
       $("#event_title").val("");
       $("#day").val("");
   }

   function createNewEventCallback(event) {
      var jsonData = JSON.parse(event.target.responseText);
      if (jsonData.eventAdded) {
          var eventname = jsonData.eventname;
          $("#userFeedbackMessage").html(eventname);
          $("#event").hide();
          updateCalendar();
      } else {
          $("#userFeedbackMessage").html("Failed to Add the Specified Event");
      }
      $('#Popup-Event-Dialog-Box').dialog("close");
   }
   
   //edit an existing event
   function editExistingEventAjax() {
       var eventname = $("#event_title").val();
       var h = document.getElementById("hour");
       var hour = h.options[h.selectedIndex].value;
       var day = "";
       var day = document.getElementById("day").value;
       var dataString = "eventname=" + encodeURIComponent(eventname) +
           "&day=" + encodeURIComponent(day) +
           "&hour=" + encodeURIComponent(hour);
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("POST", "editEvent.php", true);
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       xmlHttp.send(dataString); 
       xmlHttp.addEventListener("load", editExistingEventCallback, false);
       $("#event_title").val("");
       $("#day").val("");
   }
   
   function editExistingEventCallback(event) {
      var jsonData = JSON.parse(event.target.responseText);
      if (jsonData.eventEdit) {
          $("#event").hide();
          updateCalendar();
      }
   }
       
   //delete an existing event
   function deleteExistingEventAjax() {
       var eventname = $("#event_title").val();
       var dataString = "eventname=" + encodeURIComponent(eventname);
       var xmlHttp = new XMLHttpRequest(); 
       xmlHttp.open("POST", "deleteEvent.php", true);
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       xmlHttp.send(dataString); 
       xmlHttp.addEventListener("load", deleteExistingEventCallback, false);
       $("#event_title").val("");
       $("#day").val("");
   }

   function deleteExistingEventCallback(event) {
      var jsonData = JSON.parse(event.target.responseText);
      if (jsonData.eventDeleted) {
          $("#userFeedbackMessage").html("Successfully deleted the specified event.");
          $("#event").hide();
          updateCalendar();
      } else {
          $("#userFeedbackMessage").html("Failed to delete the specified event.");
      }
   }
       
   //retrieve the existing events to be drawn in the dialog box if the day is clicked
   function retrieveAllEvents() {
       var day = "";
       var day = document.getElementById("day").value;
       var dataString = "day=" + encodeURIComponent(day);
       
       console.log(day);
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("POST", "getEvents.php", true);
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       xmlHttp.addEventListener("load", retrieveAllEventsCallback, false);
       xmlHttp.send(dataString);
   }
   //retrieve the existing events to be drawn in the dialog box if the day is clicked
   function retrieveAllEventsCallback(event) {
       var jsonData = JSON.parse(event.target.responseText);
       var eventsParent = document.getElementById("events_for_the_day");
       eventsParent.innerHTML = "";
       if (jsonData.eventExisted) {
           var events = jsonData.events;
           for (var i in events) {
               var title = events[i].title;
               var time = events[i].time;
               eventsParent.innerHTML += "<p>" + title + " " + time +"</p>" + "<br>";
           }
       } else {
           eventsParent.innerHTML +=
               "<p>No events scheduled.</p>" +
               "<br>";
       }
   }
   
   //retrieve the existing events to be drawn in the "todays date" section
   function drawTodaysEventDisplay(){
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth(); //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10) {
          dd='0'+dd
      }
      var todays_date = yyyy + "-" + mm + "-" + dd;
      check_for_todays_events(todays_date);
   }
   //retrieve the existing events to be drawn in the "todays date" section
   function check_for_todays_events(todays_date) {
      
       var dataString = "day=" + encodeURIComponent(todays_date);
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("POST", "checkForTodaysEvents.php", true);
       xmlHttp.setRequestHeader("Content-Type",
           "application/x-www-form-urlencoded");
       xmlHttp.addEventListener("load", check_for_todays_events_Callback, false);
       xmlHttp.send(dataString);
   }
   //retrieve the existing events to be drawn in the "todays date" section
   function check_for_todays_events_Callback(event) {
       var jsonData = JSON.parse(event.target.responseText);
       $("#actualEventsToday").html("");
       //var eventsParent = document.getElementById("existingEvents");
       if (jsonData.eventExisted) {
           var events = jsonData.events;
           for (var i in events) {
               var title = events[i].title;
               var time = events[i].time;
               var eventHTML_today =
                   "<div class='todaysEventsClass'><p>" + title + " " + time + "</p>" + "<br></div>";
               //$("#todaysEventsDisplay").append(eventHTML_today);
               $("#actualEventsToday").append(eventHTML_today);
           }
       }
       else{
         var eventHTML_today =
                   "<div class='todaysEventsClass'><p>No events scheduled for today.</p>" + "<br></div>";
         //$("#todaysEventsDisplay").append(eventHTML_today);
         $("#actualEventsToday").append(eventHTML_today);
       }
       
   }
      
      
      
   document.addEventListener("DOMContentLoaded", fetchWeather, false);
   
   var fetchWeather = function(){
       
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.open("GET",  "http://classes.engineering.wustl.edu/cse330/content/weather_json.php", true);
       
       xmlHttp.addEventListener("load", fetchWeatherajaxCallback, false);
       xmlHttp.send(null);
             
   }

   function fetchWeatherajaxCallback(event){
           var jsonData = JSON.parse(event.target.responseText);
           var humidity = jsonData.atmosphere.humidity;
           var temp = jsonData.current.temp;
           var city = jsonData.location.city;
           var state = jsonData.location.state;
           
           var tomorrow_picture_URL = "http://us.yimg.com/i/us/nws/weather/gr/" + jsonData.tomorrow.code + "ds.png";	
           var dayAfterTomorrow_picture_URL = "http://us.yimg.com/i/us/nws/weather/gr/" + jsonData.dayafter.code + "ds.png";
           document.getElementById("tomorrow_image").src = tomorrow_picture_URL;
           
           document.getElementById("location").innerHTML += "<strong>"+city+"</strong> "+state;
           document.getElementById("humidity").appendChild(document.createTextNode(humidity));
           document.getElementById("temp").appendChild(document.createTextNode(temp));
   }
   document.addEventListener("DOMContentLoaded", fetchWeather, false);


   
   function check_holiday (dt_date) {
         // check simple dates (month/date - no leading zeroes)

	var n_date = dt_date.getDate(),

		n_month = dt_date.getMonth() + 1;

	var s_date1 = n_month + '/' + n_date;

		

	if (   s_date1 == '1/1') return "New Years Day";   // New Year's Day

		if ( s_date1 == '6/14') return "Flag Day";  // Flag Day

		if ( s_date1 == '7/4') return "Independence Day";   // Independence Day

		if ( s_date1 == '11/11') return "Veterans Day"; // Veterans Day

		if ( s_date1 == '12/25') return "Christmas"; // Christmas Day

	

	// weekday from beginning of the month (month/num/day)

	var n_wday = dt_date.getDay(),

		n_wnum = Math.floor((n_date - 1) / 7) + 1;

	var s_date2 = n_month + '/' + n_wnum + '/' + n_wday;

	

	if (   s_date2 == '1/3/1') return "MLK Day";  // Birthday of Martin Luther King, third Monday in January

	if (   s_date2 == '2/3/1') return "Washington's Day";  // Washington's Birthday, third Monday in February

        if (   s_date2 == '5/3/6') return "Armed Forces Day";  // Armed Forces Day, third Saturday in May

        if (   s_date2 == '9/1/1') return "Labor Day";  // Labor Day, first Monday in September

	if (   s_date2 == '10/2/1') return "Columbus Day"; // Columbus Day, second Monday in October

        if (   s_date2 == '11/4/4') return "Thanksgiving Day"; // Thanksgiving Day, fourth Thursday in November


	// weekday number from end of the month (month/num/day)

	var dt_temp = new Date (dt_date);

	dt_temp.setDate(1);

	dt_temp.setMonth(dt_temp.getMonth() + 1);

	dt_temp.setDate(dt_temp.getDate() - 1);

	n_wnum = Math.floor((dt_temp.getDate() - n_date - 1) / 7) + 1;

	var s_date3 = n_month + '/' + n_wnum + '/' + n_wday;

	if (   s_date3 == '5/1/1'  // Memorial Day, last Monday in May

	) return "Memorial Day";



	// misc complex dates

	if (s_date1 == '1/20' && (((dt_date.getFullYear() - 1937) % 4) == 0) 

	// Inauguration Day, January 20th every four years, starting in 1937. 

	) return "Inauguration Day";

		

	if (n_month == 11 && n_date >= 2 && n_date < 9 && n_wday == 2

	// Election Day, Tuesday on or after November 2. 

	) return "Election Day";

	

	return "false";
   }
   
   
  function monthToString(month) {
       switch (month) {
           case 0:               return "January";              break;
           case 1:               return "February";             break;
           case 2:               return "March";                break;
           case 3:               return "April";                break;
           case 4:               return "May";                  break;
           case 5:               return "June";                 break;
           case 6:               return "July";                 break;
           case 7:               return "August";               break;
           case 8:               return "September";            break;
           case 9:               return "October";              break;
           case 10:              return "November";             break;
           case 11:              return "December";             break;
           default:              return "Unknown Month";
       }
   }
   function dayToString(day) {
       switch (month) {
           case 0:               return "Sunday";               break;
           case 1:               return "Monday";               break;
           case 2:               return "Tuesday";              break;
           case 3:               return "Wednesday";            break;
           case 4:               return "Thursday";             break;
           case 5:               return "Friday";               break;
           case 6:               return "Saturday";             break;
           default:              return "Unknown Day";
       }
   }